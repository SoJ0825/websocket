<?php

namespace App\WebSocket\Handler;

use BeyondCode\LaravelWebSockets\WebSockets\WebSocketHandler;
use Ratchet\WebSocket\WsServerInterface;

class IOSWebSocketHandler extends WebSocketHandler implements WsServerInterface
{
    public function getSubProtocols()
    {
        return [
            'pusher-channels-protocol-7',
            'pusher-channels-protocol-6',
            'pusher-channels-protocol-5',
            'pusher-channels-protocol-4',
        ];
    }

}
