<?php

use App\Events\NewMessage;
use App\Events\PrivateMessage;
use App\WebSocket\Handler\IOSWebSocketHandler;
use BeyondCode\LaravelWebSockets\Facades\WebSocketsRouter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

WebSocketsRouter::webSocket('/ios/app/{appKey}', IOSWebSocketHandler::class);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/public-channel', function (Request $request) {
    broadcast(new NewMessage($request->input('message')));
    return response(['data' => [ 'channelName' => 'new-message', 'eventName' => NewMessage::class]]);
});

Route::get('/private-channel', function (Request $request) {
    broadcast(new PrivateMessage($request->input('message')));
    return response(['data' => [ 'channelName' => 'private-new-message', 'eventName' => PrivateMessage::class]]);
});
