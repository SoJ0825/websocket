<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Broadcaster
    |--------------------------------------------------------------------------
    |
    | This option controls the default broadcaster that will be used by the
    | framework when an event needs to be broadcast. You may set this to
    | any of the connections defined in the "connections" array below.
    |
    | Supported: "pusher", "ably", "redis", "log", "null"
    |
    */

    'default' => env('BROADCAST_DRIVER', 'null'),

    /*
    |--------------------------------------------------------------------------
    | Broadcast Connections
    |--------------------------------------------------------------------------
    |
    | Here you may define all of the broadcast connections that will be used
    | to broadcast events to other systems or over websockets. Samples of
    | each available type of connection are provided inside this array.
    |
    */

    'connections' => [

        'pusher' => [
            'driver' => 'pusher',
            'key' => env('PUSHER_SERVER_KEY'),
            'secret' => env('PUSHER_SERVER_SECRET'),
            'app_id' => env('PUSHER_SERVER_ID'),
            'options' => [
                'cluster' => env('PUSHER_SERVER_CLUSTER'),
                'encrypted' => true,
                'host' => env('PUSHER_SERVER_HOST', '127.0.0.1'),
                'port' => env('PUSHER_SERVER_PORT', 80),
                'scheme' => env('PUSHER_SERVER_USE_TLS', false) ? 'https' : 'http',
                'useTLS' => env('PUSHER_SERVER_USE_TLS', false),
            ],
        ],

        'ably' => [
            'driver' => 'ably',
            'key' => env('ABLY_KEY'),
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
        ],

        'log' => [
            'driver' => 'log',
        ],

        'null' => [
            'driver' => 'null',
        ],

    ],

];
